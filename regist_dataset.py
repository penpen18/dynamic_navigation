#!/usr/bin/env python3

import os
import sys
import json
import configparser
from collections import defaultdict
from pymongo import MongoClient
from common import *

config = configparser.ConfigParser()
config.read('config/config.ini')

#### setting
PORT_NUM = config['mongo']['port_num']
DOMAIN = config['mongo']['domain']
MONGO_URL = 'mongodb://' + DOMAIN + ':' + PORT_NUM + '/'
DB_NAME = config['mongo']['db_name']
COLLECTION = config['mongo']['collection']
JSON_DIRECTORY = config['mongo']['json_dir']
####

# connection
print("[DB info]")
print("url             : {}".format(MONGO_URL))
print("db name         : {}".format(DB_NAME))
print("collection name : {}\n".format(COLLECTION))

client = MongoClient(MONGO_URL)
db = client[DB_NAME]

# create collection for Soliton_Dataset
collection = db[COLLECTION]

def initialize():
    print("initialize DB")
    client.drop_database(db)

def register_to_db():
    print("register to DB from "+ JSON_DIRECTORY)

    for file in os.listdir(JSON_DIRECTORY):
        base,ext = os.path.splitext(file)
        f = open((JSON_DIRECTORY+'/'+file),'r')
        json_data = json.load(f)

        for j in json_data:
            j['logname']=base

        for log in json_data:
            for k, v in log.items():
                log[k] = type_converter[k](log[k])

        ret = collection.insert_many(json_data)

if __name__ == '__main__':
    if os.path.exists(JSON_DIRECTORY):
        initialize()
        register_to_db()
    else:
        print(JSON_DIRECTORY + " not found.\n")
