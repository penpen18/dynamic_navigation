$(function () {
  var search_row_counter = 0;

  $(".add-row-btn").click(function () {
    var elm = `
      <div class="row search-row">
        <div class="input-field col s3">
          <label for="key-${search_row_counter}">Key</label>
          <input type="text" name="key" id="key-${search_row_counter}">
        </div>
        <div class="input-field col s3">
          <label for="query-${search_row_counter}">Operator</label>
          <input type="text" name="query" id="query-${search_row_counter}" autocomplete="on" list="queries">
          <datalist id="queries">
            <option value="==">
            <option value="~=">
            <option value="!=">
            <option value="<">
            <option value="<=">
            <option value=">">
            <option value=">=">
          </datalist>
        </div>
        <div class="input-field col s5">
          <label for="cond-${search_row_counter}">Condition</label>
          <input type="text" name="cond" id="cond-${search_row_counter}">
        </div>
        <a class="delete-btn btn-floating waves-effect waves-light"><i class="material-icons">delete</i></a>
      </div>
    `;
    search_row_counter++;

    $(".search-rows").append(elm);

    $(".delete-btn").on("click", function () {
      this.parentElement.remove();
    });
  });

  $(".add-row-btn").click();

  $(".search-form").submit(function () {
    var data = $('.search-form').serializeArray();
    var qs = "";
    var href = window.location.href.split("/")
    var log_name = href[href.length-1];

    for (var i = 0; i < data.length; i += 3) {
      var key = data[i].value;
      var query = data[i+1].value;
      var cond = data[i+2].value;

      if (key == "" || query == "" || cond == "")
        continue;

      if (qs.length != 0)
        qs += ",";

      qs += `${key}:${query}:${cond}`;
    }

    var path = "/?fl=" + qs;
    if (log_name.length != 0) {
      path += "&ln=" + log_name;
    }

    window.location.href = path;

    return false;
  });
});

// process tree
