from collections import defaultdict

type_converter = defaultdict(lambda: str)
numeric_type_keys = ["dst", "sn", "lv", "alert", "rs", "trs",
                     "confVer", "edition", "modified", "prevConfVer", "prevEdition",
                     "prevModified", "totalMB", "freeMB", "sessionID", "psID",
                     "zoneID", "packed", "impKrnlCnt", "size", "hide", "read",
                     "write", "pe", "valNum", "valOldNum", "page", "abrt",
                     "srcPort", "dstPort", "port", "recv", "send"]

for nk in numeric_type_keys:
    type_converter[nk] = int
