<!DOCTYPE html>
<html>
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link rel="stylesheet" href="/static/css/materialize.min.css">
    <link rel="stylesheet" href="/static/css/main.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>

  <body>
    <nav>
      <div class="nav-wrapper teal">
        <a href="/" class="brand-logo">　Dynamic Navigation</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
          <li><a href="/search/{{log_name}}">Search</a></li>
          <li><a href="/tree/{{log_name}}">Process Tree</a></li>
        </ul>
      </div>
    </nav>

    <script type="text/javascript" src="/static/js/jquery.min.js"></script>
    <script type="text/javascript" src="/static/js/materialize.min.js"></script>
    <script type="text/javascript" src="/static/js/vue.js"></script>
    <script type="text/javascript" src="/static/js/liquor-tree.umd.js"></script>
    <script type="text/javascript" src="/static/js/vue-tippy.min.js"></script>

    <div class="container">
      {{!base}}
    </div>

    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="/static/js/main.js"></script>
  </body>
</html>
