% rebase('base.tpl')
<h3>{{ log_name }}</h3>

<table><tbody>
% for k, v in log.items():
  % if k == "evtsd":
  %   continue
  % end
  <tr>
   <th>{{ str(k) }}</th>
   <td>{{ str(v) }}</td>
   </tr>
% end
</tbody></table>
