% rebase('base.tpl')
<div class="row log-name-selector">
  <label>LogName Select</label>
  <select onchange="blur(); location.href= options[this.selectedIndex].value;" class="browser-default">
	% for s in log_names:
	%     if s == log_name:
    <option value=""disabled selected>{{s}}</option>
	%     else:
    <option value="/?ln={{s}}">{{s}}</option>
	% end
	% end
  </select>
</div>

<h3>{{ log_name }}</h3>

<ul class="collapsible" data-collapsible="expandable">
  % for item in log:
  <li>
    <div class="collapsible-header">
      <span class="sn">{{ item.get("sn", "None") }}</span>
      <span class="evt">{{ item.get("evt", "None") }}</span>
      <span class="subEvt">{{ item.get("subEvt", "None") }}</span>
      <span class="evtsd">{{ item.get("evtsd", "None") }}</span>
	  <span class="badge left-most"></span>
	  % if item.get('sig', '') == 'None':
	  <span class="new badge red" data-badge-caption="SigNone"></span>
	  % end
	  % if int(item.get('lv', '')) > 5:
	  <span class="new badge orange" data-badge-caption="lv">{{item.get('lv', '')}}</span>
	  % end
    </div>
    <div class="collapsible-body grey lighten-3">
      % if item.get("sn", False):
      <a href="/detail/{{log_name}}/{{item.get("sn")}}" class="waves-effect waves-light btn right">detail</a>
      % end
      <table><tbody>
      % for k, v in item.items():
        % if k in ["sn", "evt", "subEvt", "evtsd"]:
        %   continue
        % end
        <tr>
         <th>{{ str(k) }}</th>
         <td>{{ str(v) }}</td>
         </tr>
      % end
      </tbody></table>
    </div>
  </li>
  % end
</ul>

<ul class="pagination center-align">

  % if page == 1:
  <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
  % else:
  <li class="waves-effect"><a href="{{page_link[page-1]}}">
  <i class="material-icons">chevron_left</i></a></li>
  % end

  % for (i, query) in zip(page_link.keys(), page_link.values()):
  % 	if i == page:
  <li class="disabled"><a href="#!">{{i}}</a></li>
  % 	else:
  <li class="waves-effect"><a href="{{query}}">{{i}}</a></li>
  % end
  % end

  % if page == max_page:
  <li class="disabled"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
  % else:
  <li class="waves-effect"><a href="{{page_link[page+1]}}">
  <i class="material-icons">chevron_right</i></a></li>
  % end
</ul>
