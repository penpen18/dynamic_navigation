% rebase('base.tpl')

<h3>Process Tree</h3>

<div id="show-tree">
  <tree :options="treeOptions">
    <div slot-scope="{ node }" class="node-container">
      <a v-bind:href="'/detail/{{log_name}}/' + node.text[0]" style="color: black; text-decoration: none;">
      <i class="fas fa-info-circle"></i>
      </a>
      **! node.text[1] !**
    </div>
  </tree>
</div>

<script>
var app = new Vue({
  el: '#show-tree',
  data: function() {
    return {
      treeOptions: {
        fetchData(node) {
          return "/tree/json/{{log_name}}";
        }
      }
    }
  },
})
</script>
