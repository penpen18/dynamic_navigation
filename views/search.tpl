% rebase('base.tpl')

<h3>Search log</h3>

% if error_msg is not None:
<div class="card-panel red lighten-1">
{{error_msg}}
</div>
% end

<div class="row">
  <form class="search-form col s12" method="get">
    <div class="search-rows">
    </div>

    <br>

    <button class="add-row-btn btn waves-effect waves-light" type="button">Add condition
      <i class="material-icons right">add</i>
    </button>

    <button class="search-btn btn waves-effect waves-light" type="submit">Search
      <i class="material-icons right">search</i>
    </button>

  </form>
</div>
