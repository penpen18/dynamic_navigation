import configparser
import json
import re
from pymongo import MongoClient
from bottle import route, run, template
from bottle import request, static_file
from bottle import get, post, redirect, abort, response
from common import *
from functools import lru_cache

config = configparser.ConfigParser()
config.read('config/config.ini')

#### setting
##### mongo
MONGO_PORT_NUM = config['mongo']['port_num']
DOMAIN = config['mongo']['domain']
MONGO_URL = 'mongodb://' + DOMAIN + ':' + MONGO_PORT_NUM + '/'
DB_NAME = config['mongo']['db_name']
COLLECTION = config['mongo']['collection']
JSON_DIRECTORY = config['mongo']['json_dir']

##### web_srv
WEB_PORT_NUM = config['web_srv']['port_num']
HOST = config['web_srv']['host']
####

# connection
client = MongoClient(MONGO_URL)
db = client[DB_NAME]

# create collection for Soliton_Dataset
collection = db[COLLECTION]
log_names = collection.distinct("logname")

@route("/static/css/<filename>")
def css(filename):
    return static_file(filename, root="static/css")

@route("/static/js/<filename>")
def js(filename):
    return static_file(filename, root="static/js")

@route('/tree/json/<log_name>')
def ret_json(log_name):
    response.content_type = 'application/json'
    trees = gen_process_trees(log_name)
    return json.dumps(trees)

@route('/tree/<log_name>')
def show_tree(log_name):
    res = template("show_tree", log_names=log_names, log_name=log_name)
    res = res.replace("**!", "{{")
    res = res.replace("!**", "}}")
    return res

@route('/search/<log_name>')
def search(log_name):
    return template("search", log_name=log_name, log_names=log_names, error_msg=None)

@route('/detail/<log_name>/<sn>')
def log_detail(log_name, sn):
    log = collection.find({"sn": int(sn), "logname": log_name}, {"_id": 0})
    return template("log_detail", log_name=log_name, log=list(log)[0])

UNNECESSARY_LOG = [
    "ip", "mac", "type", "timeStamp", "tmid",
    "sha256", "sha1", "md5", "csid", "sessionID",
    "dst", "os", "arc", "loc", "logname"
]

def gen_process_tree(log, children):
    node = { "text": [
        log["sn"],
        log["psPath"] + "  " + log.get("cmd", "\"\"")[1:-1],
    ]}

    if len(children[log["psGUID"]]) == 0:
        return node

    node["children"] = list()

    for log in children[log["psGUID"]]:
        ch = gen_process_tree(log, children)
        node["children"].append(ch)

    return node

@lru_cache()
def gen_process_trees(log_name):
    logs = collection.find({"logname": log_name}, {"_id": 0})

    children = {}
    parent = {}
    identity = {}

    for log in logs:
        if log["evt"] != "ps" or log["subEvt"] != "start":
            continue

        ps = log.get("psGUID", None)
        pr = log.get("parentGUID", None)

        if not ps:
            continue

        if ps not in children:
            children[ps] = list()

        parent[ps] = pr
        identity[ps] = log

        if not pr:
            continue

        if pr not in children:
            children[pr] = [log]
        else:
            children[pr].append(log)

    trees = []
    for ps, pr in sorted(parent.items(), key=lambda x: int(identity[x[0]]["sn"])):
        if pr:
            continue
        tree = gen_process_tree(identity[ps], children)
        trees.append(tree)

    return trees

def remove_keys(log):
    new_log = []
    for item in log:
        for ul in UNNECESSARY_LOG:
            if ul not in item.keys():
                continue
            del item[ul]
        new_log.append(item)
    return new_log

def append_evt_specific_data(log):
    new_log = []
    for item in log:
        evt = item["evt"]
        subEvt = item["subEvt"]

        if evt == "file":
            if subEvt in ("copy", "rename", "renameDir", "renameWPD"):
                item["evtsd"] = "{src} => {dst}".format(src=item["path"], dst=item["dstPath"])
            else:
                item["evtsd"] = item["path"]
        elif evt == "ps":
            item["evtsd"] = item["psPath"]
        elif evt == "session":
            item["evtsd"] = item.get("usr", "")
            if subEvt in ("loginR", "loginRFail"):
                item["evtsd"] = "{usr}@{com}({ip}):{port}".format(usr=item["evtsd"],
                                                                  com=item["srcCom"],
                                                                  ip=item["srcIP"],
                                                                  port=item["srcPort"])
        elif evt == "dev":
            item["evtsd"] = item["dName"]
        elif evt == "reg":
            item["evtsd"] = item["path"]
        elif evt == "net":
            item["evtsd"] = "{src}:{src_port} => {dst}:{dst_port}".format(src=item.get("srcIP", ""),
                                                                          src_port=item.get("srcPort", ""),
                                                                          dst=item.get("dstIP", ""),
                                                                          dst_port=item.get("dstPort", ""))
        elif evt == "win":
            item["evtsd"] = item["psPath"]

        else:
            item["evtsd"] = ""

        new_log.append(item)
    return new_log

def gen_page_link(range,logname,raw_filt=None):
    page_link = {}
    for i in range:
        if raw_filt is not None:
            query = "/?ln=" + logname + "&pg=" + str(i) + "&fl=" + raw_filt
        else:
            query = "/?ln=" + logname + "&pg=" + str(i)
        page_link[i] = query
    return page_link

def filter_builder(raw_filt):
    raw_filt = [f.split(":") for f in raw_filt.split(",")]
    filt = {}
    for f in raw_filt:
        k, q, c = f
        if q == "==":
            filt[k] = {"$eq": type_converter[k](c)}
        elif q == "<=":
            filt[k] = {"$lte": type_converter[k](c)}
        elif q == ">=":
            filt[k] = {"$gte": type_converter[k](c)}
        elif q == "<":
            filt[k] = {"$lt": type_converter[k](c)}
        elif q == ">":
            filt[k] = {"$gt": type_converter[k](c)}
        elif q == "!=":
            filt[k] = {"$ne": type_converter[k](c)}
        elif q == "~=":
            filt[k] = {"$regex": re.compile("{}".format(type_converter[k](c)), re.IGNORECASE)}
    return filt

@route('/')
def index():
    # get query string
    try:
        log_name = request.query.get('ln', log_names[0])
    except IndexError:
        abort(500,"Please regist MongoDB from dataset using regist_dataset.py.")

    page = int(request.query.get("pg", 1))
    raw_filt = request.query.get("fl", None) # filter (e.g. fl=evt:file,subEvt:create)

    # parse filter
    if raw_filt is not None:
        try:
            filt = filter_builder(raw_filt)
        except:
            msg = "Please fill out all items."
            return template("search", log_name=log_name, log_names=log_names, error_msg=msg)
    else:
        filt = {}
    filt["logname"] = log_name

    # fix up data range
    data_num = collection.count_documents(filt)
    data_range = [(page - 1) * 20, page * 20]

    for i in range(2):
        data_range[i] = max(data_range[i], 0)
        data_range[i] = min(data_range[i], data_num)

    # find data
    log = collection.find(filt, {"_id": 0}).skip(data_range[0]).limit(20)
    log = remove_keys(log)
    log = append_evt_specific_data(log)

    # pagenation
    max_page = -(-data_num // 20) # Round up
    if max_page < 9:
        page_range = range(1,max_page+1)
    elif page > 0 and page <= 4:
        page_range = range(1,10)
    elif page > max_page-5 and page <= max_page:
        page_range = range(max_page-8,max_page+1)
    elif page <= 0:
        page = 1
        page_range = range(1,10)
    elif page > max_page:
        page = max_page
        page_range = range(max_page-8,max_page+1)
    else:
        page_range = range(page-4,page+5)

    page_link = gen_page_link(page_range, log_name, raw_filt)

    # redirect "no_result.tpl", if can't find result.
    if not page_link:
        return template("no_result", log_name=log_name)

    return template("index", log_name=log_name, log_names=log_names, log=log,
            page=page, max_page=max_page, page_link=page_link)

if __name__ == '__main__':
    run(host=HOST, port=WEB_PORT_NUM, debug=True, reloader=True)
