# Dynamic Navigation
## 概要
動的解析に役立つツール．Soliton Dataset2018をMongoDBに保存し，解析に有用な部分を抽出できる．  
主な機能として，ログ検索機能，プロセスツリー表示機能がある．

## インストール
Ubuntu上での動作を想定している．
- `sudo apt-get install python3 python3-pip mongodb`
- `git clone THIS_REPO`
- `cd dynamic_navigation`
- `pip3 install -r config/requirements.txt`
- `sudo systemctl start mongodb`

## 使い方
### 準備
MK2Logをjson形式に変換しておく必要がある．DatasetのToolsにある`mk2log.py`を
利用すること．

### 設定
`config/config.ini`に設定(ポート番号やドメイン名)を記述している．  
各自の環境に合わせて，適宜書き換えること．

### MongoDBにデータセットを保存
json形式のDatasetをMongoDBに登録するスクリプト(regist_dataset.py)を利用する．
1. ディレクトリJson(`config.ini`を書き換えた場合はこの限りではない)を作成
2. `mk2log.py`で変換されたjson形式のファイルをJsonに配置
3. `python3 regist_dataset.py`を実行

### ログの解析
#### 起動
以下に起動までの手順を示す．設定(config.iniの内容)はデフォルトとする．
1. `mk2log.py`を利用して，MK2logをjson形式に変換
2. `regist_dataset.py`を利用して，mongodbにjsonデータを保存
3. `python3 apps.py`を実行
4. ブラウザから`localhost:8080`にアクセス

#### ログ検索機能
topページ(localhost:8080/)右上のSearchから検索画面に遷移できる．  
条件として，Key, Operator, Conditionを入力し，SEARCHボタンをクリックすると検索できる．  
例えば，Keyがps，Operatorが==，Conditionがstartの場合，プロセスがスタートしたログのみを抽出して表示する．  
Operatorは，==（一致）や~=（正規表現），>=（以上）などを使用することができる．  
また，条件は複数指定することができる．

#### プロセスツリー表示機能
topページ(localhost:8080/)右上のProcess Treeからプロセスツリー表示画面に遷移できる．
子を持つプロセスの名前をクリックすると子プロセス一覧を展開できる．また，プロセス名の左にある
iマークをクリックすると，プロセス生成の詳細ページに遷移できる．
